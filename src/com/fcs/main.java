package com.fcs;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class main {

	public static void main (String[] args) {
		int n = Integer.parseInt(args[0]);
		List<String> lines = new ArrayList<>();
		Random rand = new Random();
		int upperbound = 100;
		for (int i = 0; i < n; i++) {
			lines.add(Integer.toString(rand.nextInt(upperbound)));
		}
		
		Path filePath;
		if (args.length > 1) {
			filePath = Paths.get(args[1]);
		} else {
			filePath = Paths.get("generatedFile");
		}
		try {
			Files.write(filePath, lines, StandardCharsets.UTF_8);
			System.out.println("File saved");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
